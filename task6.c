
#include <stdio.h>
#include <string.h>
#define N 12

typedef struct p{
    int placeID;
    int isFree;
    char name[80];
    char surname[80];
}place;

place Places[N];

void bubblesort()  //сортировка данных по фамилии
{
    int i, j;
    for (i = N - 1; i >= 0; i--)
        for (j = 0; j < i; j++)
            if (strcmp(Places[j].surname, Places[j + 1].surname) > 0){
                place p = Places[j];
                Places[j] = Places[j+1];
                Places[j+1] = p;
            }
}

void printMenu(){ //show menu
    printf("\nEnter 'f' to see the number of free places\n");
    printf("Enter 'l' to see the list of free places\n");
    printf("Enter 'r' to see the list of booked places sorted by surname\n");
    printf("Enter 'b' to book a place\n");
    printf("Enter 'c' to cancel reservation\n");
    printf("Enter 'q' to quit\n\n");
}

int numberFree(){
    int counter = 0;
    for (int i = 0; i < N; i++){
        if (Places[i].isFree)
            counter ++;
    }
    return counter;
}

void freeList(){
    printf("List of free places: \n");
    for (int i = 0; i < N; i++){
        if (Places[i].isFree){
            printf("Place ID: %d\n", Places[i].placeID);
        }
    }
}
void bookedList(){
    printf("List of booked places: \n");
    bubblesort();
    for (int i = 0; i < N; i++){
        if (!Places[i].isFree){
            printf("Place ID: %d, name & surname: %s %s\n", Places[i].placeID, Places[i].name, Places[i].surname);
        }
    }
}
void book(){ //book a place
    int n, j = 0;
    printf("Select place (1-12): ");
    scanf("%d", &n);
    getchar();
    if (n < 1 || n > 12){
        printf("Such place doesn't exist!\n");
        return;
    }
    else{
        for (int i = 0; i < N; i++){
            if (Places[i].placeID == n ){
                j = i;
                if(!Places[i].isFree){
                    printf("This place is already booked!\n");
                    return;
                }
                
            }
        }
        char name[80], surname[80];
        printf("Enter your name: ");
        scanf("%s", name);
        getchar();
        printf("Enter your surname: ");
        scanf("%s", surname);
        getchar();
        printf("Confirmation: \n%s %s, do you want to book place number %d? Enter 'y' to confirm, any other character to quit: ", name, surname, n);
        char res;
        scanf("%c", &res);
        getchar();
        if(res == 'y'){
            
            strcpy(Places[j].name, name);
            strcpy (Places[j].surname, surname);
            Places[j].isFree = 0;
            Places[j].placeID = n;
            printf("Information was saved!\n");
            return;
        }
        else{
            printf("Information wasn't saved!\n");
            
        }
    }
}
void cancel(){ //cancel reservation
    int j;
    printf("Enter number of your place: ");
    scanf("%d", &j);
    getchar();
    int flag = 0;
    if (j >= 1 && j <= 12){
        for (int i = 0; i < N; i++){
            if (Places[i].placeID == j && !Places[i].isFree)
            {
                printf("Confirmation: \n%s %s, do you want to cancel your reservation of place %d? Enter 'y' to confirm, any other carachter to quit: ", Places[i].name, Places[i].surname, j);
                char res;
                scanf("%c", &res);
                getchar();
                if (res == 'y'){
                    Places[i].name[0] = '\0';
                    Places[i].surname[0] = '\0';
                    Places[i].isFree = 1;
                    printf("Reservation was cancelled\n");
                    flag = 1;
                }
            }
        }
        if (!flag)
            printf("Information wasn't saved!\n");
    }
    else
        printf("Invalid data!\n");
}
int main(){
    FILE *info = fopen("FlightInfo.bin", "rb");
    if (info == NULL){
        for (int i = 0; i < N; i++){
            Places[i].isFree = 1;
            Places[i].placeID = i + 1;
            Places[i].name[0] = '\0';
            Places[i].surname[0] = '\0';
        }
    }
    else{
        fread(Places, sizeof(place), N, info);
        fclose(info);
    }
    
    while(1){
        printMenu();
        char c;
        scanf("%c", &c);
        getchar();
        switch(c){
            case 'f': printf("Total number of free places: %d\n", numberFree()); break;
            case 'l': freeList(); break;
            case 'r': bookedList(); break;
            case 'b': book(); break;
            case 'c': cancel(); break;
            case 'q':
            {
                FILE *out = fopen("FlightInfo.bin", "wb");
                fwrite(Places, sizeof(place), N, out);
                fclose(out);
                return 0;
            };
            default: printf("%c : Invalid character!\n", c);
        }
    }
}


